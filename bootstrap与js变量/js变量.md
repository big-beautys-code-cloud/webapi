**字面量**
不会变的值，如 123
变量的值其实就是字面量

变量就是用符号来表示这些字面量

**变量的定义**

var name='moumou';

let name ='moumou'

**var和let区别**

var 有变量提升

let不能在定义之前访问该变量，但是var可以。

```
    var name;
    console.log(name);
    name='javascrpt';
    let age;
```

作用域不一样

```

   if(1){
        var result2=false;
        
    }
    console.log(result2)


    if(1){
        let result=false;
        funciton aa(){

        }
    }
    console.log(result)
```

var 可以重复申明的，let 不行

参考 https://m.php.cn/article/482937.html

**作业** 腾讯面试题，还有没别的方案？？？？？




