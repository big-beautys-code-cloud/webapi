**表单**

form-group 逐行显示 form-inline 一行显示

**模态框:经过了优化，更加灵活，以弹出对话框的形式出现，具有最小和最实用的功能集**

基于 class  modal fade，button 跟 模态框的关联通过 data-target，大框的显示使用class modal-lg

小框的显示 modal-sm

务必为 .modal 添加 role="dialog" 和 aria-labelledby="..." 属性，用于指向模态框的标题栏；为 .modal-dialog 添加 aria-hidden="true" 属性。

另外，你还应该通过 aria-describedby 属性为模态框 .modal 添加描述性信息。