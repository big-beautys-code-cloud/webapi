**前端子使用原理**

媒体查询

**版本选择**

选择稳定版，当前的稳定版3

**bootstarp 样式是根据绑定的class 来显示的**

**栅格系统**

bootstrap 提供了一套响应式、移动设备优先的流式栅格系统，随着屏幕或视口（viewport）尺寸的增加，系统会自动分为最多12列

**栅格系统针对设备的简写**

xs extra Small 超小
sm small
md medium
lg large
xl extra Large 超大

**栅格系统的class名称的前缀**

超小屏   col-xs
小屏幕   col-sm
电脑屏（中等屏幕）col-md
大屏     col-lg
超大屏   col-xl

名称的位数为数值，改数值表示的在12等分中占用了几份

如果每行的尾数相加的数值加上不为12，会空出省份的份数

如果每行元素的尾数相加的数值超过了12，会展示为多行

如果要实行偏移，可以使用offset,类名的格式为 col-(设备的简写如 md)-offset-（偏移的数值 如 1,2,3）

**嵌套列**

bootstrap 的嵌套的宽度划分都是针对父类，把父类12等分，类名中如 col-md-6 ,占用的份数是6，宽度是针对父类而言

**列排序**
```
<div class="row">
  <div class="col-md-9 col-md-push-3">.col-md-9 .col-md-push-3</div>
  <div class="col-md-3 col-md-pull-9">.col-md-3 .col-md-pull-9</div>
</div>
```
push 往右推，push后面的数值为推得份数
pull 往左拉，pull后面的数值为拉的份数





